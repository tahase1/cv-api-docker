FROM python:3.9

WORKDIR /fastapi_cv_docker

COPY ./requirements.txt /fastapi_cv_docker/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /fastapi_cv_docker/requirements.txt

COPY ./src /fastapi_cv_docker/src

EXPOSE 80

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "80"]